<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_projects', function (Blueprint $table) {
            $table->id();
            //step 1
            $table->string('title');
            $table->longText('intro');
            $table->longText('description');

            //step 2
            $table->string('img_title');
            $table->string('image');
            $table->string('img_desc');

            //step3
            $table->string('goal');

            //step 4
            $table->string('opt_title');
            $table->string('opt_amount');
            $table->string('opt_desc');

            //step 5
            $table->string('owner_name');
            $table->string('email');
            $table->string('owner_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_projects');
    }
}
