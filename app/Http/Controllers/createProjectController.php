<?php

namespace App\Http\Controllers;

use App\Models\userProject;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class createProjectController extends Controller
{
    public function create(Request $request){

        $path = "";
        if($request->img){
            $name = time() . '.'  . explode('/',explode(':',substr($request->img,0,strpos($request->img,';')))[1])[1];

            $img = Image::make($request->img);
            $img->save(public_path('uploads/').$name);
            $path = 'uploads/' . $name;
        }

       $project = userProject::create([
           'title'=>$request->title,
           'intro'=>$request->intro,
           'description'=>$request->description,
           'img_title'=>$request->img_title,
           'img_desc'=>$request->img_desc,
           'image'=>$path,
           'goal'=>$request->goal,
           'opt_title'=>$request->opt_title,
           'opt_amount'=>$request->opt_amount,
           'opt_desc'=>$request->opt_desc,
           'owner_name'=>$request->owner_name,
           'email'=>$request->email,
           'owner_info'=>$request->owner_info,
       ]) ;
        return response()->json($project,200);

    }
    public function getProject(){
        $userProject = userProject::orderBy('created_at', 'desc')->first();
       return response()->json($userProject,200);
    }
}
