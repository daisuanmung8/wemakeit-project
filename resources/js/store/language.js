
       const lang = {

           en: {
               step1: "Intro Text",
               step2: "Upload Image",
               step3: "Financial Goal",
               step4: "Pledge Options",
               step5: "Owner Information",
               title: "Title",
               intro: "Intro",
               description: "Description",
               img_title: "Image Name",
               img_desc: "Image Description",
               goal: "Set Your Goal",
               opt_title: "Title",
               opt_amount: "Set your amount",
               opt_desc: "Options Description",
               owner_name: "Owner Name",
               email: "Email",
               owner_info: "Owner Information"
           },

           de: {
               step1: "Einführungstext",
               step2: "Bild hochladen",
               step3: "Finanzielles Ziel",
               step4: "Optionen",
               step5: "Besitzerinformation",
               title: "Titel",
               intro: "Einführung",
               description: "Beschreibung",
               img_title: "Bild Name",
               img_desc: "Bildbeschreibung",
               goal: "Setzen Ihr Ziel",
               opt_title: "Titel",
               opt_amount: "Stellen Sie Ihren Betrag ein",
               opt_desc: "Options Beschreibung",
               owner_name: "Besitzername",
               email: "Email",
               owner_info: "Besitzer Information"
           }
       }


  export  default lang
